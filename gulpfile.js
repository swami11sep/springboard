/*--  Variables  --*/
var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

/*-- Stream Task  --*/
gulp.task('stream', function () {
    gulp.src(['assets/sass/custom/custom.scss'])
            .pipe(sass({errLogToConsole: true}))
            .pipe(concat('style.css'))
            .pipe(gulp.dest('./assets/css'))
            .pipe(minifyCSS())
            .pipe(concat('style.min.css'))
            .pipe(gulp.dest('./assets/css'));
});

/*--  Watch Task  --*/
gulp.task('watch', function () {
    watch('assets/sass/**/*.scss', function () {
        gulp.start('stream');
    });
    watch(['assets/js/validations.js'], function () {
        gulp.start('compress');
    });
});

/*--  Compress Task  --*/
gulp.task('compress', function () {
    gulp.src([
        'assets/js/validations.js'
    ])
            .pipe(concat('main.js'))
            .pipe(gulp.dest('./assets/js'))
            .pipe(uglify())
            .pipe(concat('main.min.js'))
            .pipe(gulp.dest('./assets/js'));
});

/*-- Default Function  --*/
gulp.task('default', function () {
    /*--  place code for your default task here  --*/
    gulp.task('default', ['compress', 'stream']);
});
